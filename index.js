
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
    //first function here:
function userInfo(){
    let userName = prompt("Enter your Full Name " );
    let userAge = prompt("Enter your Age" );
    let userLocation = prompt("Enter your Location  " );

    console.log("Hello, " + userName);
    console.log("You are " + userAge + " years old");
    console.log("You live in " + userLocation + " City");
}
userInfo();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:
function myFavorites(){
    let firstBand = "Bts";
    let secondBand = "Imagine";
    let thirdBand = "One republic";
    let fourthBand = "The yellow diaries";
    let fifthBand = "Aerosmith";

    console.log("1. " + firstBand);
    console.log("2. " + secondBand);
    console.log("3. " + thirdBand);
    console.log("4. " + fourthBand);
    console.log("5. " + fifthBand);
}
myFavorites();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function myMovies(){
    let firstMovie = "Puss in Boots";
    let secondMovie = "The Menu";
    let thirdMovie = "The Fabelmans";
    let fourthMovie = "Devotion";
    let fifthMovie = "Aftersun";

    let firstMovieRTR = 93;
    let secondMovieRTR = 76;
    let thirdMovieRTR = 82;
    let fourthMovieRTR = 92;
    let fifthMovieRTR = 81;


    console.log("1. " + firstMovie);
    console.log("Rotten Tomatoes Rating: " + firstMovieRTR + "%");

    console.log("2. " + secondMovie);
    console.log("Rotten Tomatoes Rating: " + secondMovieRTR + "%");

    console.log("3. " + thirdMovie);
    console.log("Rotten Tomatoes Rating: " + thirdMovieRTR + "%");

    console.log("4. " + fourthMovie);
    console.log("Rotten Tomatoes Rating: " + fourthMovieRTR + "%");

    console.log("5. " + fifthMovie);
    console.log("Rotten Tomatoes Rating: " + fifthMovieRTR + "%");
}
myMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);